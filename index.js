function guardarDatosUsuario(){
  var nombre = document.getElementById("txtNombre").value;
  var email = document.getElementById("txtEmail").value;
  var dni = document.getElementById("txtDni").value;

  localStorage.setItem("nombre", nombre);
  localStorage.setItem("email", email);
  localStorage.setItem("dni", dni);

  sessionStorage.setItem("nombre",nombre);
  sessionStorage.setItem("email",email);
  sessionStorage.setItem("dni",dni);

  var usuario = {"nombre": nombre, "email": email,"dni": dni}
  localStorage.setItem("usuarioST",JSON.stringify(usuario));
  sessionStorage.setItem("usuarioST",JSON.stringify(usuario));
}

function recuperarDatosUsuario(){
  var nombrelocal = localStorage.getItem("nombre");
  var emaillocal = localStorage.getItem("email");
  var dnilocal = localStorage.getItem("dni");

  var nombresession = sessionStorage.getItem("nombre");
  var emailsession = sessionStorage.getItem("email");
  var dnisession = sessionStorage.getItem("dni");

  //metodo 2
  var usuarioRecLoc = localStorage.getItem("usuarioST");
  var usuarioRecSes = sessionStorage.getItem("usuarioST");

  //alert
  alert("local user: " + JSON.parse(usuarioRecLoc).email);
  alert("local nombre: "+nombrelocal + "session nombre" + nombresession);
  alert("local email: "+emaillocal + "session email" + emailsession);
  alert("local dni: "+dnilocal + "session dni" + dnisession);
}
